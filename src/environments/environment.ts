// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAbEu_mU3PtcVjCQmQQgIfW_L092bajZ3o",
    authDomain: "fireside-14f7f.firebaseapp.com",
    projectId: "fireside-14f7f",
    storageBucket: "fireside-14f7f.appspot.com",
    messagingSenderId: "350833586168",
    appId: "1:350833586168:web:2144a95ad5bc370cf5b00d",
    measurementId: "G-0G8E3235NZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
