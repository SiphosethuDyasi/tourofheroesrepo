import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  registered = false

  register(email: string, password: string): void {

    var userId = Math.floor(Math.random() * 100) + 1;

    interface User {
      userID: number,
      email: string,
      password: string;
    }

    email = email.trim();
    password = password.trim();

    let user: User = { userID: userId, email: email, password: password }
    let userDetailsArray: User[] = [];

    userDetailsArray = JSON.parse(localStorage.getItem('userDetails') || '[]');
    userDetailsArray.push(user);
    localStorage.setItem('userDetails', JSON.stringify(userDetailsArray));


    this.registered = true;
    this.router.navigate(['/login'])
      .then(() => {
        window.location.reload();
      });


  }

}
