import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  constructor(private router: Router) { }

  ngOnInit(): void {

  }

  logOut(): void {
    localStorage.setItem('loggedIn', 'false');
    this.router.navigate(['/login'])
      .then(() => {
        window.location.reload();
      });
  }

}
