import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../shared/authentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {

  }

  registered = true;

  check = JSON.parse(localStorage.getItem('loggedIn') || '')

  logIn(email: string, password: string): void {
    const userDetails = JSON.parse(localStorage.getItem('userDetails') || '[]');

    for (var x = 0; x < userDetails.length; x++) {
      if (userDetails[x].email === email && userDetails[x].password === password) {

        localStorage.setItem('loggedIn', 'true');
        this.check = JSON.parse(localStorage.getItem('loggedIn') || '');
        this.router.navigate(['/dashboard'])
          .then(() => {
            window.location.reload();
          });
        return
      } else {
        console.log('incorrect username and password')
      }
    }
  }

  openRegister() {
    this.registered = false;
    this.router.navigate(['/register']);
  }

}
